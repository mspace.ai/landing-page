FROM node:16
WORKDIR /app
RUN addgroup --gid 1001 nodejs
RUN adduser --system nextjs -u 1001
RUN yarn add next@^10.0.0 next-pwa@^3.1.4 react@^17.0.0 react-dom@^17.0.0
COPY ./package.json /app
RUN mkdir /app/contracts
COPY ./contracts /app/contracts
RUN yarn
COPY . .
RUN yarn build && rm -Rf /app/src
USER nextjs
EXPOSE 3000
CMD ["npx", "next", "start"]

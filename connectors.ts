import { InjectedConnector } from "@web3-react/injected-connector";
import { defaultChains } from "lib/constants";

export const injected = new InjectedConnector({
  supportedChainIds: defaultChains,
});

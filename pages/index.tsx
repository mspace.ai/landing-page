import Header from "components/Layout1/Header"
import { Image } from "@chakra-ui/react"
import { useState, useRef } from "react"

export default function Index() {

  const [email, setEmail] = useState('')
  const warningRef = useRef(null)
  const successRef = useRef(null)

  function handleChange(e: any) {
    setEmail(e.target.value)
  }

  function sendEmail() {
    if (validateEmail(email)) {
      var formData = new FormData()
      formData.append('email', email)
      fetch("https://script.google.com/macros/s/AKfycbwbVuGFp5RRyi2Q-zEHbILv4-6RRAtXFHEEphIeCljHI6XdIwDXGZKR3-6wQgDnQuZg/exec", { method: 'POST', body: formData })
        .then(response => {
          if (null !== successRef.current) {
            successRef.current.style.opacity = 1
          }
          setEmail('')
          setTimeout(() => {
            successRef.current.style.opacity = 0
          }, 3000);
        })
        .catch(error => console.error('Error!', error.message))
    } else {
      if (null !== successRef.current) {
        warningRef.current.style.opacity = 1
      }
      setTimeout(() => {
        warningRef.current.style.opacity = 0
      }, 3000);
    }
  }

  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  return (
    <div className="comming-soon">
      <Header />
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <div className="title">COMING SOON</div>
        <div className="sub-title-1">MSPACE is a Metadium-based Swap protocol designed as to exploring<br
          className="break-line" /> POA <span className="note-text">(protocol owned assets)</span> mechanism and
          introducing new forms of DAO.</div>
        <div className="progress">
          <div className="title-progress">START</div>
          <div>
            <div className="title-progress" style={{ opacity: 1, textAlign: 'center', marginBottom: 12, fontWeight: 400 }}>Ignition countdown</div>
            <div className="progress-bound">
              <div className="progress-bar">
                <div className="progress-dot">
                </div>
              </div>
            </div>
          </div>
          <div className="title-progress">OPEN</div>
        </div>

        <div className="box-input">
          <div className="input-bound">
            <input id="input-email" className="input-email" placeholder="Enter a valid email address" value={email} onChange={handleChange} />
          </div>
          <div className="button-send" onClick={sendEmail}>
            NOTIFY ME
          </div>
        </div>

        <div className="sub-title-2">Subscribe for upcoming news.</div>
        <div className="hyperlink">
          <a href="https://t.me/mspace_ai" target="_blank" rel="noreferrer">
            <Image src="images/telegram.svg" style={{ width: 30, height: 30, margin: '0 11px' }} />
          </a>
          <a href="https://discord.gg/qqxyEPFq" target="_blank" rel="noreferrer">
            <Image src="images/discord.svg" style={{ width: 30, height: 30, margin: '0 11px' }} />
          </a>
          <a href="https://twitter.com/@mspace_ai" target="_blank" rel="noreferrer">
            <Image src="images/twitter.svg" style={{ width: 30, height: 30, margin: '0 11px' }} />
          </a>
        </div>
      </div>

      <div className="alert warning" id="alert-warning" ref={warningRef}>
        The email field must be a valid email.
      </div>
      <div className="alert success" id="alert-success" ref={successRef}>
        Your email has been saved.
      </div>
    </div>
  )
}
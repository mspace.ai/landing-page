import { Web3ReactProvider } from "@web3-react/core";
import type { AppProps } from "next/app";
import getLibrary from "lib/getLibrary";
import 'styles/index.css'
import NoLayout from 'components/NoLayout'
import Layout1 from 'components/Layout1'
import Layout2 from 'components/Layout2'
import LanguageProvider from 'language';

const layouts = {
  L1: Layout1,
  L2: Layout2,
};

function NextWeb3App({ Component, pageProps }: AppProps) {

  const MyPageContext = Component as any
  const Layout = layouts[MyPageContext.layout] || NoLayout

  return (
    <Web3ReactProvider getLibrary={getLibrary}>
      <LanguageProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </LanguageProvider>
    </Web3ReactProvider>
  );
}

export default NextWeb3App;

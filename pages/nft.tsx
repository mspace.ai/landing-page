import { useCallback, useState } from 'react';
import { Trans } from '@lingui/react'
import MintedModal from 'components/modals/MintedModal'
import SocialNavigation from 'components/common/SocialNavigation';
import { ImArrowDown } from 'react-icons/im'
import { Box, Container, Image } from '@chakra-ui/react';
import { useAppContext } from 'contexts/AppContext';
import MintModal from 'components/modals/MintModal';
import CommingSoonModal from 'components/modals/QuickModal';
import { utils } from 'ethers';
import { LoadingModal } from 'components/modals/LoadingModal';
import { getTokenLogo } from 'lib/helpers';
import { useWeb3React } from '@web3-react/core';

function NFT() {
  const { chainId } = useWeb3React()
  const { price, maxSupply, totalSupply } = useAppContext();

  const tokenLogo = getTokenLogo(chainId)
  const [showModal, setShowModal] = useState(false);
  const [cmsModal, setCmsModal] = useState(false);
  const onClose = useCallback(() => {
    setShowModal(false);
  }, []);
  return (
    <Box
      as="main"
      className="nft-page"
      bg={{
        base: 'url(/images/nft.png) no-repeat top center'
      }}
    >
      <Container maxW="container.lg">
        <div className="intro">
          <h2 className="page-title"><Trans id="Get your SCB!" /></h2>
          <p><Trans id="You can mint SCB or Space Construction Bot who can mine Mineral, MSP token and build constructions along the roadmap." /></p>
        </div>
        <div className="rounds">
          <div className="round r1st">
            <label>1st Round</label>
            <div className="box">
              <div className="media">
                <div className="img" style={{ opacity: 1 }}>
                  <Image src="/images/r1.png" />
                </div>
              </div>
              <div className="detail">
                <small>{totalSupply} / {maxSupply}</small>
                <div className="acc">
                  <span className="balance">
                    <Image src={tokenLogo} /> {utils.formatEther(price || 0)}
                  </span>
                  <span className="fluc"><ImArrowDown />20%</span>
                </div>
              </div>
            </div>
            <button onClick={() => setShowModal(true)}>Claim</button>
          </div>
          <div className="round r2nd">
            <label>2nd Round</label>
            <div className="box">
              <div className="media">
                {/* <span className="countdown">00.02:36:34</span> */}
                <div className="img">?</div>
              </div>
              <div className="detail">
                <small>3333 / 3333</small>
                <div className="acc">
                  <span className="balance">
                    <Image src={tokenLogo} /> 900
                  </span>
                  <span className="fluc"><ImArrowDown />10%</span>
                </div>
              </div>
            </div>
            <button onClick={() => setCmsModal(true)}>Claim</button>
          </div>
          <div className="round r3rd">
            <label>3rd Round</label>
            <div className="box">
              <div className="media">
                <span className="status soldout">SOLD OUT</span>
              </div>
              <div className="detail">
                <small>3333 / 3333</small>
                <div className="acc">
                  <span className="balance">
                    <Image src={tokenLogo} /> 1000
                  </span>
                  <span className="fluc"></span>
                </div>
              </div>
            </div>
            <button onClick={() => setCmsModal(true)}>Claim</button>
          </div>
        </div>
      </Container>
      <SocialNavigation />
      <MintModal open={showModal} onClose={onClose} />
      <MintedModal />
      <LoadingModal />
      <CommingSoonModal
        title="Comming soon!"
        show={cmsModal}
        onClose={() => setCmsModal(false)}
        cancelLabel="OK"
      />
    </Box>
  )
}

NFT.layout = "L2"

export default NFT
import { Trans } from '@lingui/react'
// import Link from 'next/link'
import { Image } from "@chakra-ui/react"
import { useState } from 'react'
import Modal from 'components/modals/QuickModal'

function Home() {
  const [showCommingSoon, setShowCommingSoon] = useState(false)
  return (
    <main>
      <div className="container">
        <div className="navigation">
          <div className="item">
            <div className="wrap">
              <div className="content">
                <h3 className="title">
                  <Trans
                    id="Miner NFT Drop <0></0> Event"
                    components={[<br key={1} />]}
                  />
                </h3>
                <p className="desc">
                  <Trans
                    id="Get Special Miner NFT & <0></0> Join the community"
                    components={[<br key={2} />]}
                  />
                </p>
              </div>
              <button onClick={() => setShowCommingSoon(true)}>
                <a>Join
                  <Image src="/images/right.svg" width={30} height={30} />
                </a>
              </button>
            </div>
          </div>
          <div className="item">
            <div className="wrap">
              <div className="content">
                <h3 className="title">
                  <Trans
                    id="Explore <0>Metadium</0><1></1>swap protocol"
                    components={[<span key={3} />, <br key={4} />]}
                  />
                </h3>
                <p className="desc"></p>
              </div>
              <a href={process.env.SWAP_HOME_URL}>
                <Image src="/images/left.svg" width={32} height={32} />
                <span>Beta</span>
                Launch APP
              </a>
            </div>
          </div>
        </div>
        <div className="features">
          <h2>Features & Roadmap</h2>
          <div className="feature-items">
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/community.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Community</h3>
                  <p><Trans id="Become a part of MSPACE community by getting your SCB miners!SCBs can propose and vote for new changes and contribute to the development of MSPACE community." /></p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/swap.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Trade</h3>
                  <p><Trans id="Exchange META with MSP minerals or other MRC-20 tokens supported byMetadium network." /></p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/pool.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Pool</h3>
                  <p><Trans id="Combine two pair tokens in 50% each and let the fusion pools earn you extra tokens." /></p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/mining.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Mining</h3>
                  <p><Trans id="Start mining MSP minerals and use the minerals to craft your MSPACE." /></p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/bank.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Bank</h3>
                  <p><Trans id="Deposit your MSP minerals in MSPACE Bank vault that let your mienrals compile more." /></p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="wrap">
                <div className="media">
                  <Image src="/images/spacecraft.png" alt="" />
                </div>
                <div className="desc">
                  <h3 className="title">Spacecraft</h3>
                  <p><Trans id="Participate in the creation of new projects and contribute MSP minerals to enable the initial project launching events." /></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        title={"NFT Sale will be soon!"}
        show={showCommingSoon}
        onClose={() => setShowCommingSoon(false)}
        cancelLabel="OK"
      />
    </main>
  )
}

Home.layout = "L1"

export default Home

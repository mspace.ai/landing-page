import {
  Grid,
  GridItem,
  Heading,
  Accordion,
  AccordionItem,
  AccordionButton,
  Box,
  AccordionIcon,
  AccordionPanel,
  Input,
} from '@chakra-ui/react';
import { PrimaryButton } from 'components/common/BaseButton';
import GalleryItem from 'components/common/GalleryItem'

function Gallery() {
  const list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  const filters = ['BACKGROUND', 'EYES', 'FACE', 'ITEMS']
  return (
    <Box
      as="main"
      py={10}
      background={{
        base: 'url(/images/m-gallery.webp) no-repeat left bottom / 100%',
        md: 'url(/images/gallery.png) no-repeat left bottom / contain',
      }}
      paddingBottom={{
        base: 300,
        md: 450
      }}
    >
      <div className="container">
        <Grid templateColumns='repeat(4, 1fr)' gap={12}>
          <GridItem colSpan={{ base: 4, lg: 1 }}>
            <aside>
              <Heading as="h1" fontSize={30} mb={2}>GALLERY</Heading>
              <Accordion mb={4}>
                {filters.map((item, i) => (
                  <AccordionItem borderTopWidth={i ? 1 : 0} key={i}>
                    <h2>
                      <AccordionButton _focus={{ outline: 'none' }}>
                        <Box flex='1' textAlign='left'>
                          {item}
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                    </AccordionPanel>
                  </AccordionItem>
                ))}
              </Accordion>
              <Input
                mb={8}
                borderWidth={0}
                borderBottomWidth={1}
                borderRadius="none"
                placeholder='By ID'
                _focus={{
                  outline: "unset"
                }}
              />
              <PrimaryButton w="100%">Reset Filter</PrimaryButton>
            </aside>
          </GridItem>
          <GridItem colSpan={{ base: 4, lg: 3 }}>
            <section>
              <Grid templateColumns='repeat(3, 1fr)' gap={8}>
                {list.map((item, i) => (
                  <GridItem w='100%' key={i} colSpan={{ base: 3, lg: 1 }}>
                    <GalleryItem />
                  </GridItem>
                ))}
              </Grid>
            </section>
          </GridItem>
        </Grid>
      </div>
    </Box>
  )
}
Gallery.layout = "L2"

export default Gallery

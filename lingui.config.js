module.exports = {
  catalogs: [
    {
      path: '<rootDir>/language/locales/{locale}/messages',
      include: ['<rootDir>/pages', '<rootDir>/components'],
      exclude: ['**/node_modules/**'],
    },
  ],
  // compileNamespace: 'cjs',
  // extractBabelOptions: {},
  fallbackLocales: {},
  format: 'minimal',
  formatOptions: { origins: false, lineNumbers: false },
  sourceLocale: 'en',
  locales: [
    'en',
    'ko',
  ],
  orderBy: 'messageId',
  pseudoLocale: '',
  rootDir: '.',
  runtimeConfigModule: {
    i18n: ['@lingui/core', 'i18n'],
    Trans: ['@lingui/react', 'Trans'],
  },
}
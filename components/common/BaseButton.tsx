import { Button } from '@chakra-ui/react'

export function PrimaryButton(props: any) {
  return (
    <Button
      h="auto"
      py={4}
      px={12}
      borderRadius={0}
      backgroundImage="linear-gradient(167deg, #4ec8fb -70%, #500c5d 113%)"
      _hover={{
        backgroundImage: "linear-gradient(167deg, #4ec8fb -70%, #500c5d 113%)",
        outline: "none",
      }}
      _focus={{
        outline: "none",
      }}
      {...props}
    >{props.children}</Button>
  )
}

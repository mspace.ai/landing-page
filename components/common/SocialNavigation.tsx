import { Image } from '@chakra-ui/image'
import React from 'react'

export default function SocialNavigation() {
  return (
    <footer className="layout2">
      <div className="hyperlink">
        <a href="https://t.me/mspace_ai" target="_blank" rel="noreferrer">
          <Image src="/images/telegram.svg" height={30} width={30} />
        </a>
        <a href="https://discord.gg/qqxyEPFq" target="_blank" rel="noreferrer">
          <Image src="/images/discord.svg" height={30} width={30} />
        </a>
        <a href="https://twitter.com/@mspace_ai" target="_blank" rel="noreferrer">
          <Image src="/images/twitter.svg" height={30} width={30} />
        </a>
      </div>
    </footer>
  )
}

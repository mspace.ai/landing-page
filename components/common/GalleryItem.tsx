import { Box, Flex, Image } from '@chakra-ui/react'

function GalleryItem({ src, id, tx }: any) {
  return (
    <Box className="gallery-item">
      <Image src={src} alt="" />
      <Flex
        direction="column"
        alignItems="center"
        justifyContent="center"
        bg="#18082e"
        p={3}
        pb={5}
      >
        <span>Metalien ID 0</span>
        <Box
          fontSize={12}
        >Tx 0a00032013341….4124214</Box>
      </Flex>
    </Box>
  )
}

export default GalleryItem
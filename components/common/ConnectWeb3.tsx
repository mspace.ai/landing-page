import { Flex, Text } from '@chakra-ui/react';
import { Trans } from '@lingui/react';
import React, { useState } from 'react';
import { Web3Provider } from '@ethersproject/providers';
import { UnsupportedChainIdError, useWeb3React } from '@web3-react/core';
import { InjectedConnector } from '@web3-react/injected-connector';
import { MetamaskRequiredModal } from 'components/modals/MetamaskRequiredModal';
import { PrimaryButton } from 'components/common/BaseButton';
import { useRenderChain } from 'hooks/useRenderChain';
import { WalletFilledIcon } from 'icons/WalletFilledIcon';
import { defaultChain, defaultChains } from 'lib/constants';
import useMetaMaskOnboarding from 'hooks/useMetaMaskOnboarding';

const ConnectWeb3 = () => {
  const injectedConnector = new InjectedConnector({ supportedChainIds: defaultChains, })
  const { activate, active, deactivate, error } = useWeb3React<Web3Provider>()
  const [showModal, setShowModal] = useState(false);
  const { isMetaMaskInstalled } = useMetaMaskOnboarding();
  const isUnsupportedChainIdError = error instanceof UnsupportedChainIdError

  const renderChain = useRenderChain()

  const handleConnect = () => {
    if (isMetaMaskInstalled) {
      activate(injectedConnector)
    } else {
      setShowModal(true);
    }
  };

  return (
    <Flex
      minHeight="100vh"
      alignItems="center"
      justifyContent="center"
    >
      <Flex
        background="white"
        boxShadow="0px 1rem 2rem rgba(204, 218, 238, 0.8)"
        borderRadius="1rem"
        direction="column"
        align="center"
        w="calc(100% - 2rem)"
        p="2rem"
        maxW="31rem"
        mx={4}
      >
        <Flex
          bg="primary"
          borderRadius="50%"
          p="1rem"
          justify="center"
          align="center"
          color="black"
          mb={4}
        >
          <WalletFilledIcon boxSize={8} color="#595ea0" />
        </Flex>
        {/* {loading ? (
          <Text color="black" fontSize="xl" fontWeight="bold" mb={4}>
            <Trans id="Connecting Wallet" />
          </Text>
        ) : (
          <> */}
        <Text color="black" fontSize="xl" fontWeight="bold" mb={4}>
          <Trans id={isUnsupportedChainIdError ? `Switch your network` : 'Connect Wallet'} />
        </Text>
        {!isUnsupportedChainIdError && !active && (
          <Text color="gray.700" mb={4} textAlign="center">
            <Trans id="To get started, connect your wallet" />
          </Text>
        )}
        {isUnsupportedChainIdError ? (
          <>
            <Text color="gray.700" mb={4} textAlign="center">
              <Trans
                id="To access Mspace, please switch to {network}"
                values={{
                  network: renderChain(defaultChain),
                }}
              />
            </Text>
            <PrimaryButton
              minW={200}
              onClick={deactivate}
            >
              <Trans id="Disconnect" />
            </PrimaryButton>
          </>
        ) : (
          <PrimaryButton
            minW={200}
            onClick={handleConnect}
          // isLoading={loading}
          >
            <Trans id="Connect" />
          </PrimaryButton>
        )}
      </Flex>
      <MetamaskRequiredModal
        isOpen={showModal}
        onClose={() => setShowModal(false)}
      />
    </Flex >
  )
}

export default ConnectWeb3

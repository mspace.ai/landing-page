import Head from 'next/head'

function IndexPage() {
  return (
    <Head>
      <title>Mspace</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta name="description" content="Be a friendly Metalien miner that can mine minerals to make a new metaverse in space." />
      <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
    </Head>
  )
}

export default IndexPage

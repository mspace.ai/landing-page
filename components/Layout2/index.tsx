import React, { useMemo } from 'react'
import { Web3Provider } from '@ethersproject/providers';
import { useWeb3React } from '@web3-react/core';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import ConnectWeb3 from 'components/common/ConnectWeb3';
import IndexPage from '../IndexPage';
import Header from './Header';
import { AppProvider } from 'contexts/AppContext';

const theme = extendTheme({
  styles: {
    global: {
      body: {
        color: '#FFFFFF',
        fontSize: 14,
        fontFamily: "'Noto Sans', sans-serif",
        position: 'relative',
        minHeight: '100vh',
        backgroundImage: 'linear-gradient(134deg, #2f1e57 3%, #2d1c53 21%, #160633 42%)',
        // backgroundImage: 'url(/images/bg.png), linear-gradient(134deg, #2f1e57 3%, #2d1c53 21%, #160633 42%)',
        // backgroundSize: 'contain',
        // backgroundRepeat: 'no-repeat',
      },
    },
  },
  colors: {
    modalBG: 'rgba(66, 47, 95, 0.72)',
  }
})

export default function Layout2({ children }) {
  const context = useWeb3React<Web3Provider>()
  const { account, active, error } = context

  const valid = useMemo(() => !!account && active && !error,
    [
      account,
      active,
      error,
    ],
  );
  return (
    <ChakraProvider theme={theme}>
      <AppProvider>
        <IndexPage />
        <Header />
        {valid ? children : <ConnectWeb3 />}
      </AppProvider>
    </ChakraProvider>
  )
}

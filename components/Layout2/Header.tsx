import React from 'react'
import { useWeb3React } from "@web3-react/core";
import { IoGridSharp } from 'react-icons/io5';
import useEagerConnect from "hooks/useEagerConnect";
import LangSwitcher from '../LangSwitcher'
import Logo from '../Logo'
import Account from "../Account";
import AccountBalance from "../AccountBalance";
import { Link, Text, Flex } from '@chakra-ui/layout';
import { Trans } from '@lingui/react';
import { Icon } from '@chakra-ui/react';

export default function Header() {
  const { account, library } = useWeb3React();

  const triedToEagerConnect = useEagerConnect();

  const isConnected = typeof account === "string" && !!library;

  return (
    <header className="layout2">
      <div className="container-fluid">
        <div className="flex justifyContent-between alignItems-center">
          <Logo />
          <div className="btn-minting">
            <Flex
              display="inline-flex"
              alignItems="center"
              justifyContent="center"
              w={6}
              h={6}
              borderRadius="50%"
              bg="#FFF"
              color="blackAlpha.600"
              fontSize={10}
              fontWeight="bold"
              verticalAlign="middle"
              mr={2}
            >NFT</Flex>
            <Text display={{ base: 'none', md: 'inline' }}>MINTING</Text>
          </div>
          <div>
            <Link href="/gallery" style={{ display: 'flex', alignItems: 'center' }}>
              <Icon as={IoGridSharp} mr={2} fontSize={18} />
              <Text fontWeight="bold" display={{ base: 'none', md: 'inline' }}>
                <Trans id="GALLERY" />
              </Text>
            </Link>
          </div>
          <div className="header-right">
            <section className="account">
              {isConnected && (
                <AccountBalance />
              )}
              <Account triedToEagerConnect={triedToEagerConnect} />
            </section>
            <LangSwitcher />
          </div>
        </div>
      </div>
    </header>
  )
}

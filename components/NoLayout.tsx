import React from 'react'

export default function NoLayout({ children }) {
  return (
    <div className="no-layout">
      {children}
    </div>
  )
}

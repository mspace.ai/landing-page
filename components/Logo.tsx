import React from 'react'
import Link from 'next/link'
import { Image } from '@chakra-ui/image'

export default function Logo() {
  return (
    <h1 className="logo">
      <Link href="/">
        <a>
          <Image src="/images/logo.svg" />
        </a>
      </Link>
    </h1>
  )
}

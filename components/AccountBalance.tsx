import type { Web3Provider } from "@ethersproject/providers";
import { useWeb3React } from "@web3-react/core";
import useAccountBalance from "../hooks/useAccountBalance";
import { parseBalance } from "../util";

const AccountBalance = () => {
  
  const { account } = useWeb3React<Web3Provider>();
  const { balance, symbol } = useAccountBalance(account);
  
  return <p className="wallet-balance">{parseBalance(balance ?? 0)} {symbol}</p>;
};

export default AccountBalance;

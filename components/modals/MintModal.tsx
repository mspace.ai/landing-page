import {
  Flex,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import { Trans } from '@lingui/react';
import { PrimaryButton } from 'components/common/BaseButton';
import { useAppContext } from 'contexts/AppContext';
import { useState, useEffect } from 'react';

function MintModal({ open, onClose }) {
  const { mint } = useAppContext();
  const [isOpen, setOpen] = useState(true);

  useEffect(() => {
    setOpen(open)
  }, [open]);

  function handleMint() {
    mint()
    onClose()
  }

  return (
    <Modal
      isCentered
      size="full"
      isOpen={isOpen}
      onClose={onClose}
      motionPreset='slideInBottom'
    >
      <ModalOverlay background="modalBG">
        <ModalContent
          boxShadow="0px 1rem 2rem #617492"
          borderRadius="0"
        >
          <ModalBody
            px={6}
            py={0}
            bgColor="#100922"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Flex
              direction="column"
              alignItems="center"
              alignSelf="stretch"
              px={{ base: 10, md: 115 }}
              pt={`calc((100vh - 500px) / 2)`}
              background="url(/images/mint-effect.png) no-repeat top center / 100%"
              maxWidth="100%"
            >
              <Text fontSize={40} fontWeight="bold" mb={9}>
                <Trans id="Mint your NFT" />
              </Text>
              <Flex
                w="360px"
                borderWidth={2}
                borderStyle="solid"
                p={5}
                backgroundColor='#0f0a22'
                direction="column"
                alignItems="center"
                style={{
                  borderImageSource: `linear-gradient(138deg, #da019d 1%, #00e0fd 28%, #bcad55 92%`,
                  borderImageSlice: 1,
                }}
              >
                <Flex justifyContent="flex-end" w="100%" mb={4}>
                  <ModalCloseButton
                    position="relative"
                    outline="none"
                    size="xs"
                    color="white"
                    right={0}
                    top={0}
                    p={1}
                    _focus={{
                      outline: 'none',
                    }}
                  />
                </Flex>
                <Image
                  h={240}
                  w={240}
                  mb={7}
                  src="/images/r1.png"
                />
                <PrimaryButton
                  w="100%"
                  onClick={handleMint}
                >
                  <Trans id="Mint" />
                </PrimaryButton>
              </Flex>
            </Flex>
          </ModalBody>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  )
}

export default MintModal

import {
  Flex,
  Image,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  Link,
} from '@chakra-ui/react';
import { Trans } from '@lingui/react';
import { useWeb3React } from '@web3-react/core';
import { useAppContext } from 'contexts/AppContext';
import { getExplorerUrl } from 'lib/helpers';
import { useState, useEffect } from 'react';
import { getTransactionString } from '../../util';

function MintedModal() {
  const { chainId } = useWeb3React()
  const { loading, token, clearToken } = useAppContext();
  const explorerUrl = getExplorerUrl(chainId);
  const [isOpen, setOpen] = useState(false);

  useEffect(() => {
    if (token && !loading) {
      setOpen(true)
    }
    return () => setOpen(false)
  }, [loading, token]);

  function handleClose() {
    clearToken()
  }

  return (
    <Modal
      isCentered
      size="full"
      isOpen={isOpen}
      onClose={handleClose}
      motionPreset='slideInBottom'
    >
      <ModalOverlay background="modalBG">
        <ModalContent
          boxShadow="0px 1rem 2rem #617492"
          borderRadius="0"
        >
          <ModalBody
            px={6}
            py={0}
            bgColor="#100922"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Flex
              direction="column"
              alignItems="center"
              alignSelf="stretch"
              px={{ base: 10, md: 115 }}
              pt={`calc((100vh - 500px) / 2)`}
              background="url(/images/mint-effect.png) no-repeat top center / 100%"
              maxWidth="100%"
            >
              <Text fontSize={40} fontWeight="bold" mb={9}>
                <Trans id="Minted!" />
              </Text>
              <Flex
                w="360px"
                borderWidth={2}
                borderStyle="solid"
                p={5}
                backgroundColor='#0f0a22'
                direction="column"
                alignItems="center"
                style={{
                  borderImageSource: `linear-gradient(138deg, #da019d 1%, #00e0fd 28%, #bcad55 92%`,
                  borderImageSlice: 1,
                }}
              >
                <Flex justifyContent="flex-end" w="100%" mb={4}>
                  <ModalCloseButton
                    position="relative"
                    outline="none"
                    size="xs"
                    color="white"
                    right={0}
                    top={0}
                    p={1}
                    _focus={{
                      outline: 'none',
                    }}
                  />
                </Flex>
                <Image
                  h={240}
                  w={240}
                  mb={7}
                  src={token?.image}
                />
                <Flex direction="column" alignItems="center">
                  <Text color="white" fontSize={22}>ID {token?.id}</Text>
                  <Link
                    href={`${explorerUrl}/tx/${token?.hash}`}
                    target="_blank"
                    fontSize={18}
                    color="blue.400"
                  >TxID {getTransactionString(token?.hash)}</Link>
                </Flex>
              </Flex>
            </Flex>
          </ModalBody>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  )
}

export default MintedModal

import {
  Flex,
  Image,
  Modal,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import { Trans } from '@lingui/react';
import { useAppContext } from 'contexts/AppContext';
import React from 'react';

export const LoadingModal = () => {
  const { loading } = useAppContext();

  return (
    <Modal
      isOpen={loading}
      closeOnEsc={false}
      closeOnOverlayClick={false}
      onClose={null}
      isCentered
    >
      <ModalOverlay background="modalBG">
        <ModalContent background="none" boxShadow="none" borderRadius="0">
          <Flex direction="column" align="center" justify="center">
            <Image src={'images/loading.png'} mb={4} w={100} />
            <Text color="white" fontWeight="bold">
              <Trans id="Loading" />...
            </Text>
          </Flex>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  );
};

import { Component, useEffect, useRef, useState } from 'react';
import {
  Flex,
  Button,
  Modal,
  ModalBody,
  // ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';

interface ModalProps {
  title: String;
  children?: Component | String;
  cancelLabel?: String | 'OK';
  show: boolean;
  onClose: () => void;
}

export default (props: ModalProps) => {
  const { title, children, cancelLabel, show, onClose } = props
  const [open, setOpen] = useState(show)

  useEffect(() => {
    setOpen(show)
  }, [show])

  const cancelButtonRef = useRef(null)

  return (
    <Modal
      isCentered
      // size="full"
      isOpen={open}
      onClose={onClose}
    // motionPreset='slideInBottom'
    >
      <ModalOverlay>
        <ModalContent
          bg="unset"
          maxW="xs"
        >
          <ModalBody
            p={6}
            borderRadius={10}
            bgColor="gray.700"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Flex
              direction="column"
              alignItems="center"
              alignSelf="stretch"
              w="100%"
            >
              {title && (
                <Text fontSize={18} fontWeight={500} mb={6}>
                  {title}
                </Text>
              )}
              {/* <Flex justifyContent="flex-end" w="100%" mb={4}>
                <ModalCloseButton
                  position="relative"
                  outline="none"
                  size="xs"
                  color="white"
                  right={0}
                  top={0}
                  p={1}
                  _focus={{
                    outline: 'none',
                  }}
                />
              </Flex> */}

              {children && (
                <div className="mt-2">
                  {children}
                </div>
              )}
              <Button
                isFullWidth
                color="black"
                fontWeight="normal"
                onClick={onClose}
                ref={cancelButtonRef}
                _focus={{
                  outline: 'none'
                }}
              >
                {cancelLabel}
              </Button>
            </Flex>
          </ModalBody>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  )
}
import {
  Flex,
  Link,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  Image,
} from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';

const MetamaskRequiredModal = ({ isOpen, onClose }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  return (
    <>
      <Modal isOpen={open} isCentered onClose={onClose}>
        <ModalOverlay background="rgba(98, 118, 148, 0.9)">
          <ModalContent
            boxShadow="0px 1rem 2rem #617492"
            mx={{ base: 12, lg: 0 }}
            maxW={{ base: '20rem', md: '25rem' }}
          >
            <ModalHeader padding="16px 20px 0" bg="white" borderRadius={4}>
              <Flex alignItems="center" direction="column" m="auto">
                <Image src={`/images/metamask-fox.svg`} width={45} alt="MetaMask" />
                <Text
                  fontSize={{ base: 16, md: 24 }}
                  fontWeight="bold"
                  textAlign="center"
                  color="black"
                >
                  MetaMask
                </Text>
              </Flex>
            </ModalHeader>
            <ModalBody px={5} pt={4} pb={{ base: 0, md: 8 }}>
              <Flex align="center" direction="column" textAlign="center">
                <Link href="https://metamask.io/" target="_blank" color="black">
                  Install
                </Link>
                <Text mb={2} color="gray.400">
                  Please, try again after installing the wallet in your browser
                </Text>
              </Flex>
            </ModalBody>
          </ModalContent>
        </ModalOverlay>
      </Modal>
    </>
  );
};

export { MetamaskRequiredModal };

import { Image } from '@chakra-ui/image'
import React from 'react'

export default function Footer() {
  return (
    <footer>
      <div className="container-fluid">
        <div className="wrap">
          <a href="#">MSPACE Term&Conditions</a>
          <div className="copyright">
            <Image src="/images/flogo.svg" height={80} width={60} />
            <span>@2021 MSPACE</span>
          </div>
          <div className="hyperlink">
            <a href="https://t.me/mspace_ai" target="_blank" rel="noreferrer">
              <Image src="/images/telegram.svg" height={30} width={30} />
            </a>
            <a href="https://discord.gg/qqxyEPFq" target="_blank" rel="noreferrer">
              <Image src="/images/discord.svg" height={30} width={30} />
            </a>
            <a href="https://twitter.com/@mspace_ai" target="_blank" rel="noreferrer">
              <Image src="/images/twitter.svg" height={30} width={30} />
            </a>
          </div>
        </div>
      </div>
    </footer>
  )
}

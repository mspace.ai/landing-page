import React from 'react'
import IndexPage from '../IndexPage'
import Footer from './Footer'
import Header from './Header'

export default function Layout1({ children }) {
  return (
    <>
      <IndexPage />
      <Header />
      {children}
      <Footer />
    </>
  )
}

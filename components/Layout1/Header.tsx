import React from 'react'
import LangSwitcher from '../LangSwitcher'
import Logo from '../Logo'

export default function Header() {
  
  return (
    <header>
      <div className="container-fluid">
        <div className="flex justifyContent-between">
          <Logo />
          <LangSwitcher />
        </div>
      </div>
    </header>
  )
}

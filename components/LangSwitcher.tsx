import { useLanguageData } from '../language/hooks'

export default function LangSwitcher() {
  const { language, setLanguage } = useLanguageData()
  return (
    <div className="language-switcher">
      <div>
        <span className={language === 'en' ? 'active' : ''} onClick={() =>setLanguage('en')}>ENG</span>
      </div>
      <div className="divider" />
      <div>
        <span className={language === 'ko' ? 'active' : ''} onClick={() =>setLanguage('ko')}>KOR</span>
      </div>
    </div>
  )
}

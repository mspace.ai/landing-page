import type { BigNumberish } from "@ethersproject/bignumber";
import { formatUnits } from "@ethersproject/units";

export function shortenHex(hex: string, length = 4) {
  return `${hex.substring(0, length + 2)}…${hex.substring(
    hex.length - length
  )}`;
}

const SCAN_PREFIXES = {
  1: "etherscan.io",
  11: "explorer.metadium.com",
  12: "explorer.metadium.com",
  42: "kovan.etherscan.io",
};

export function formatEtherscanLink(
  type: "Account" | "Transaction",
  data: [number, string]
) {
  switch (type) {
    case "Account": {
      const [chainId, address] = data;
      return `https://${SCAN_PREFIXES[chainId]}/address/${address}`;
    }
    case "Transaction": {
      const [chainId, hash] = data;
      return `https://${SCAN_PREFIXES[chainId]}/tx/${hash}`;
    }
  }
}

export const parseBalance = (
  value: BigNumberish,
  decimals = 18,
  decimalsToDisplay = 3
) => parseFloat(formatUnits(value, decimals)).toFixed(decimalsToDisplay);

export const getTransactionString = hash => {
  if (!hash) return '';
  const len = hash.length;
  return `0x${hash.substr(2, 4)}...${hash.substr(len - 4, len - 1)}`;
};

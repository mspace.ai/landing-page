import { chainUrls, nativeCurrencies, networkNames } from './constants'

export const getNativeCurrency = chainId => nativeCurrencies[chainId || 1];

export const logError = (...args) => {
  // eslint-disable-next-line no-console
  console.error(...args);
};

export const getNetworkName = chainId =>
  networkNames[chainId] || 'Unknown Network';

const IMPOSSIBLE_ERROR =
  'Unable to perform the operation. Reload the application and try again.';

const TRANSACTION_REPLACED_ERROR =
  'Transaction was replaced by another. Reload the application and find the transaction in the history page.';

export const handleWalletError = (error, showError) => {
  if (error?.message && error?.message.length <= 120) {
    showError(error.message);
  } else if (
    error?.message &&
    error?.message.toLowerCase().includes('transaction was replaced')
  ) {
    showError(TRANSACTION_REPLACED_ERROR);
  } else {
    showError(IMPOSSIBLE_ERROR);
  }
};

export const getRPCUrl = (chainId, returnAsArray = false) =>
  returnAsArray ? chainUrls[chainId || 1].rpc : chainUrls[chainId || 1].rpc[0];

export const getTokenLogo = chainId => {
  const nativeCurrency = nativeCurrencies[chainId]
  if (!nativeCurrency) {
    return ''
  }
  return `/images/${nativeCurrency.symbol.toLowerCase()}.png`
}

export const getExplorerUrl = chainId => chainUrls[chainId]?.explorer

export const nativeCurrencies = {
  1: {
    chainId: 1,
    decimals: 18,
    name: 'Ether',
    symbol: 'ETH',
    mode: 'NATIVE',
  },
  11: {
    chainId: 11,
    decimals: 18,
    name: 'Metadium',
    symbol: 'META',
    mode: 'NATIVE',
  },
  12: {
    chainId: 12,
    decimals: 18,
    name: 'Metadium',
    symbol: 'META',
    mode: 'NATIVE',
  },
  3: {
    chainId: 3,
    decimals: 18,
    name: 'Ether',
    symbol: 'ETH',
    mode: 'NATIVE',
  },
  42: {
    chainId: 42,
    decimals: 18,
    name: 'Kovan Ether',
    symbol: 'ETH',
    mode: 'NATIVE',
  },
  56: {
    chainId: 56,
    decimals: 18,
    name: 'Binance Coin',
    symbol: 'BNB',
    mode: 'NATIVE',
  },
};

export const networkNames = {
  1: 'ETH Mainnet',
  11: 'Metadium',
  12: 'Metadium Testnet',
  3: 'Ropsten',
  42: 'Kovan Testnet',
  56: 'Binance Smart Chain',
  77: 'Sokol Testnet',
  97: 'Binance Testnet',
  100: 'xDai Chain',
};

export const chainUrls = {
  1: {
    rpc: process.env.ETHEREUM_RPC_URL?.split(' '),
    explorer: 'https://etherscan.io',
    chainId: 1,
    name: networkNames[1],
  },
  11: {
    rpc: process.env.METADIUM_RPC_URL?.split(' '),
    explorer: 'https://explorer.metadium.com',
    chainId: 11,
    name: networkNames[11],
  },
  12: {
    rpc: process.env.METADIUM_RPC_URL?.split(' '),
    explorer: 'https://testnetexplorer.metadium.com',
    chainId: 12,
    name: networkNames[12],
  },
};

export const defaultChains = [1, 4, 11, 12]
export const defaultChain = parseInt(process.env.DEFAULT_CHAIN_ID) || 1

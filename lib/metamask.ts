import { utils } from 'ethers';
import {
  getNetworkName,
  getRPCUrl,
  logError,
} from 'lib/helpers';

export const addTokenToMetamask = async ({ address, symbol, decimals }) => {
  if (typeof window !== 'undefined' && window.ethereum) {
    window.ethereum.request({
      method: 'wallet_watchAsset',
      params: {
        type: 'ERC20',
        options: {
          address,
          symbol,
          decimals,
        },
      },
    });
  }
}

const trySwitchChain = async chainId =>
  window.ethereum.request({
    method: 'wallet_switchEthereumChain',
    params: [
      {
        chainId: utils.hexValue(chainId),
      },
    ],
  });

const tryAddChain = async (chainId) =>
  window.ethereum.request({
    method: 'wallet_addEthereumChain',
    params: [
      {
        chainId: utils.hexValue(chainId),
        chainName: getNetworkName(chainId),
        rpcUrls: [getRPCUrl(chainId)],
      },
    ],
  });

export const addChainToMetaMask = async chainId => {
  const add = ![1, 3, 4, 5, 42].includes(chainId);
  if (add) {
    try {
      await tryAddChain(chainId);
      return true;
    } catch (addError) {
      logError({ addError });
    }
    return false;
  }

  try {
    await trySwitchChain(chainId);
    return true;
  } catch (switchError) {
    // This error code indicates that the chain has not been added to MetaMask.
    if (switchError.code === 4902) {
      try {
        await tryAddChain(chainId);
        return true;
      } catch (addError) {
        logError({ addError });
      }
    } else {
      logError({ switchError });
    }
  }
  return false;
};

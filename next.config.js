// @ts-check

/**
 * @type {import('next').NextConfig}
 **/

 const linguiConfig = require('./lingui.config.js')

 const { locales, sourceLocale } = linguiConfig

module.exports = {
  webpack: (config) => {
    config.module.rules = [
      ...config.module.rules,
      {
        resourceQuery: /raw-lingui/,
        type: 'javascript/auto',
      },
    ]

    return config
  },
  reactStrictMode: true,
  swcMinify: true,
  distDir: 'build',
  i18n: {
    localeDetection: true,
    locales,
    defaultLocale: sourceLocale,
  },
  env: {
    SWAP_HOME_URL: process.env.REACT_APP_SWAP_HOME_URL,
    METADIUM_RPC_URL: process.env.REACT_APP_METADIUM_RPC_URL,
    DEFAULT_CHAIN_ID: process.env.REACT_APP_DEFAULT_CHAIN_ID,
    NFT_ADDRESS: process.env.REACT_APP_NFT_ADDRESS,
    BASE_URL: process.env.REACT_APP_BASE_URL,
  }
};

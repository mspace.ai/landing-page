import { useWeb3React } from "@web3-react/core";
import { BigNumber, Contract, ethers } from "ethers";
import React, { useContext, useEffect, useState } from "react";
import MINER_ABI from "contracts/NFTERC20.json";
import { useToast } from "@chakra-ui/react";

type Token = {
  id: BigNumber;
  hash: string;
  image: string;
};

interface AppContext {
  price?: BigNumber;
  totalSupply?: number;
  maxSupply?: number;
  mint: Function;
  token?: Token,
  clearToken: Function;
  loading?: boolean;
}

export const AppContext = React.createContext<AppContext>({
  token: null,
  mint: async (h) => !!h,
  clearToken: () => null,
})

export const useAppContext = () => useContext(AppContext)

export const AppProvider = ({ children }) => {
  const { account, library, chainId } = useWeb3React()
  const [totalSupply, setTotalSupply] = useState<number>()
  const [maxSupply, setMaxSupply] = useState<number>()
  const [price, setPrice] = useState<BigNumber>()
  const [readContract, setReadContract] = useState<Contract>()
  const [token, setToken] = useState<Token>()
  const [loading, setLoading] = useState<boolean>(false)
  const toast = useToast()

  useEffect(() => {
    (async () => {
      if (library && account) {
        const tokenAddress = process.env.NFT_ADDRESS
        const contract = new Contract(tokenAddress, MINER_ABI, library.getSigner(account));

        setReadContract(contract)

        setPrice((await contract.price()))

        setTotalSupply((await contract.totalSupply()).toNumber());

        setMaxSupply((await contract.maxSupply()).toNumber());

        contract.on('Transfer', async (from, to, id, event) => {
          setLoading(false)
          setTotalSupply((await contract.totalSupply()).toNumber());
          // const tokenURI = await contract.tokenURI(id);
          // const tokenURI = `${process.env.BASE_URL}/${chainId}/json/${id}.json`
          const imageURI = `${process.env.BASE_URL}/${chainId}/images/${id}.png`
          setToken({
            id: id.toNumber(),
            hash: event.transactionHash,
            image: imageURI,
          })
        });
      }
    })();

  }, [library])

  const mint = async () => {
    setLoading(true)
    try {
      const tx = await (await readContract.mint({ value: price })).wait();
      const filterMint = readContract.filters.Transfer(ethers.constants.AddressZero, account);
      await readContract.queryFilter(filterMint, tx.blockNumber);
    } catch (error) {
      setLoading(false)
      toast({
        title: ' Error',
        description: error.message,
        status: 'error',
        isClosable: true,
      })
    }
    return true
  }

  const clearToken = () => {
    setToken(null)
  }

  return <AppContext.Provider
    value={{
      price,
      totalSupply,
      maxSupply,
      mint,
      token,
      clearToken,
      loading,
    }}
  >
    {children}
  </AppContext.Provider>
}
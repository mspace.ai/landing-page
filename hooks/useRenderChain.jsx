import React, { useCallback } from 'react';
import { Badge, Text, Tooltip, Image } from '@chakra-ui/react';
import { useSwitchChain } from 'hooks/useSwitchChain';
import { defaultChains } from 'lib/constants';
import { getNetworkName } from 'lib/helpers';
import useMetaMaskOnboarding from './useMetaMaskOnboarding';

export const useRenderChain = () => {
  const { isMetaMaskInstalled } = useMetaMaskOnboarding();
  const switchChain = useSwitchChain();

  const renderChain = useCallback(
    chainId => {
      const networkName = getNetworkName(chainId);
      const isDefaultChain = defaultChains.includes(chainId);
      const isMobileBrowser = navigator?.userAgent?.includes('Mobile') || false;
      const buttonWillWork = isMetaMaskInstalled && (isMobileBrowser ? !isDefaultChain : true);

      return buttonWillWork ? (
        <Tooltip label={`Click to switch to ${networkName}`} position="auto">
          <Badge
            display="inline-flex"
            alignItems="center"
            py={1}
            px={2}
            m={1}
            borderRadius={5}
            size="1"
            cursor="pointer"
            colorScheme="blue"
            onClick={() => switchChain(chainId)}
          >
            <Image src={`/images/metamask-fox.svg`} width={20} />
            <Text ml={2} as="span">{networkName}</Text>
          </Badge>
        </Tooltip>
      ) : (
        <Badge
          display="inline-flex"
          alignItems="center"
          py={1}
          px={2}
          m={1}
          borderRadius={5}
          size="1"
          colorScheme="blue"
        >
          {networkName}
        </Badge>
      );
    },
    [switchChain, isMetaMaskInstalled],
  );

  return renderChain;
};

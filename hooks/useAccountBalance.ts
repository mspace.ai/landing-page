import type { Web3Provider } from "@ethersproject/providers";
import { useWeb3React } from "@web3-react/core";
import useSWR from "swr";
import { getNativeCurrency } from "../lib/helpers";
import useKeepSWRDataLiveAsBlocksArrive from "./useKeepSWRDataLiveAsBlocksArrive";

function getAccountBalance(library: Web3Provider) {
  return async (_: string, address: string) => {
    return await library.getBalance(address);
  };
}

export default function useAccountBalance(address: string, suspense = false) {
  const { library, chainId } = useWeb3React();

  const shouldFetch = typeof address === "string" && !!library;

  const result = useSWR(
    shouldFetch ? ["AccountBalance", address, chainId] : null,
    getAccountBalance(library),
    {
      suspense,
    }
  );

  const currency = getNativeCurrency(chainId);
  
  useKeepSWRDataLiveAsBlocksArrive(result.mutate);


  return { balance: result?.data, symbol: currency.symbol };
}
